package defaultPackage;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import org.junit.Test;

@SuppressWarnings("unused")
public class Exercice1 {
	
	public interface Observer
	{
		public void update(String message);
	}
	
	public interface Observable
	{
		public void enregistrerObserver(Observer observer);
		public void supprimerObserver(Observer observer);
		public void notifierObservers();
	}

	public static class MessageBoard implements Observable
	{
		
		private ArrayList<Observer> tablObservers = new ArrayList<Observer>(); 
		private String message;
		private boolean changed= false;
		
		public MessageBoard(String message)
		{
			this.message=message;
			this.changed=false;
		};
		
		@Override
		public void enregistrerObserver(Exercice1.Observer observer) {
			tablObservers.add(observer);
		}
		@Override
		public void supprimerObserver(Exercice1.Observer observer) {
			tablObservers.remove(observer);	
		}
		@Override
		public void notifierObservers() {
			if(this.changed==true)
			{
				for(Observer c1: tablObservers)
					c1.update(this.message);
				this.changed=false;
			}	
		}
		
		public void setMessage(String message)
		{
			this.message=message;
			this.changed=true;
			this.notifierObservers();
		}		
		public void setMessageByUser(Observer c1, String message)
		{
			for(int i=0;i<this.tablObservers.size();i++)
			{
				if (c1==this.tablObservers.get(i))
				{
					setMessage(message);
				}
			}
		}			
	}

	public static class BoardReaders implements Observer
	{
		private int nbBoardReaders;
		private String latestMessage;
		
		public BoardReaders(int nbBoardReaders) {
			this.nbBoardReaders = nbBoardReaders;
		}
		
		@Override
		public void update(String message) {
			System.out.println(this.nbBoardReaders + " : " + message);
		}
		
        public String getLatestMessage() {
            return latestMessage;
        }
	}
	
	public static void main(String args[]) 
	{	
		MessageBoard mb = new MessageBoard("Clear");
		BoardReaders c1 = new BoardReaders(1);
		BoardReaders c2 = new BoardReaders(2);
		mb.enregistrerObserver(c1);
		mb.enregistrerObserver(c2);
		mb.setMessage("nouveau message");
		mb.setMessage("nouveau message2");
		mb.setMessageByUser(c1, "message user");
    	MessageBoard messageBoard = new MessageBoard("initial message");

    	//Test
        BoardReaders boardReader1 = new BoardReaders(1);
        BoardReaders boardReader2 = new BoardReaders(2);

        messageBoard.enregistrerObserver(boardReader1);
        messageBoard.enregistrerObserver(boardReader2);

        messageBoard.setMessage("new message");
        assertEquals("new message", boardReader1.getLatestMessage());
        assertEquals("new message", boardReader2.getLatestMessage());

        messageBoard.setMessageByUser(boardReader1, "user message");
        assertEquals("user message", boardReader1.getLatestMessage());
        assertEquals("user message", boardReader2.getLatestMessage());
    }
}