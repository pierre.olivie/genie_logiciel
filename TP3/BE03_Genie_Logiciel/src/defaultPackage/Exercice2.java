package defaultPackage;

import java.util.Observable;
import java.util.Observer;
import java.util.*;  


public class Exercice2
{
	public static class Table extends Observable
	{
		private int tab[];
		private String car;
		
		public Table(int length, String car)
		{
			
			this.tab = new int[length];
			tab[0]=1;
			for(int i =1;i<this.tab.length;i++)
			{
				tab[i]=0;
			}
			this.car = car;
		}
		
		
		public void right()
		{
			for (int i=0;i<this.tab.length;i++)
			{
				
				
				if (this.tab[i]==1)
				{
					
					tab[(i+1)%this.tab.length]=tab[i];
					tab[i]=0;
					break;
				}	
			}
		}
		
		public void left()
		{
			for (int i=0;i<this.tab.length;i++)
			{
				if (i==0 && this.tab[i]==1)
				{
					tab[0]=0;
					tab[this.tab.length-1]=1;
					break;
				}
				
				if (this.tab[i]==1)
				{
					tab[(i-1)%this.tab.length]=tab[i];
					tab[i]=0;
					break;
				}	
			}
		}

		public int[] getTab() {
			return tab;
		}

		public void setTab(int[] tab) {
			this.tab = tab;
		}


		public String getCar() {
			return car;
		}


		public void setCar(String car) {
			this.car = car;
		}
		
		
	}
	
	
	public static class View implements Observer
	{
		
		public Table table;
		
		public View(Table table)
		{
			super();
			this.table = table;
			this.table.addObserver(this);
		}
		
		public void viewA(Table table)
		{
			for(int i=0; i<table.getTab().length;i++)
			{
				System.out.print("+---");
			}	
			System.out.print("+\n");
			
			for(int i=0; i<table.getTab().length;i++)
			{
				if (table.getTab()[i]==1)
				{
					System.out.print("| * ");
				}
				else
				{
					System.out.print("|   ");
				}	
			}
			System.out.print("|\n");
			for(int i=0; i<table.getTab().length;i++)
			{
				System.out.print("+---");
			}	
			System.out.print("+\n");
		}
		
		public void viewB(Table table)
		{
			for(int i=0; i<table.getTab().length;i++)
			{
				System.out.print(" + ");
			}	
			System.out.print("\n");
			
			for(int i=0; i<table.getTab().length;i++)
			{
				System.out.print("/ \\");
			}	
			System.out.print("\n");
			
			for(int i=0; i<table.getTab().length;i++)
			{
				if (table.getTab()[i]==1)
				{
					System.out.print("+");
					System.out.print(table.getCar());
					System.out.print("+");
				}
				else
				{
					System.out.print("+ +");
				}	
			}
			System.out.print("+\n");
			
			for(int i=0; i<table.getTab().length;i++)
			{
				System.out.print("\\ /");
			}	
			System.out.print("\n");
			
			for(int i=0; i<table.getTab().length;i++)
			{
				System.out.print(" + ");
			}	
			System.out.print("\n");		
		}
		
		@Override
		public void update(Observable o, Object arg) 
		{
			this.viewA(this.table);
		}
	}
	
	public static class Player
	{
		private Table table;
		private View view;
		
		public Player(Table table,View view)
		{
			this.table=table;
			this.view=view;
		}
		
		public void left()
		{
			this.table.left();
		}
		
		public void right()
		{
			this.table.right();
		}
		
		public void updateView(String viewOf)
		{
			//this.view.viewA(this.table);
			if(viewOf.equals("viewA"))
			{
				this.view.viewA(this.table);
			}
			else
			{
				this.view.viewB(this.table);
				
			}
		}
	}
	
	public static void main(String args[]) 
	{
		
		Table table = new Table(5,"*");
		View view = new View(table);
		Player player = new Player(table,view);
		Boolean flag = true;
		String message;
		Scanner sc = new Scanner(System.in); 
		while(flag)
		{
			System.out.println("Enter a command ['left', 'right', 'viewA', 'viewB', 'stop']"); 
			message= sc.next(); 
			switch(message)
			{
			case "left":
				player.left();
				break;
			case "right":
				player.right();
				break;
			case "viewA":
				player.updateView(message);
				break;
			case "viewB":
				player.updateView(message);
				break;
			case "stop":
				flag=false;
				break;
			}
			
			
		}
	}
}