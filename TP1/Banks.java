package defaultPackage;

import java.util.ArrayList;

public class MainClassExercice1 {
	//Classe comprenant l'ensemble de l'exercice 1
	
	public static abstract class Account
	{
		private static int cptAccount; //va permettre l'incrémentation des numéros de comptes.
		private int accountNumber = 0;
		private int ownerNumber;
		private int balance = 0;
		
		public Account(int OwnerNumber, int balance)
		{	//Constructeur de Account
			this.ownerNumber = ownerNumber;
			this.accountNumber = cptAccount;
			this.balance = balance;
			cptAccount ++;
		}
		//Géneration des getters/setters nessessaires
		public int getOwnerNumber() {
			return ownerNumber;
		}
		public void setOwnerNumber(int ownerNumber) {
			this.ownerNumber = ownerNumber;
		}

		public int getBalance() {
			return balance;
		}
		public void setBalance(int balance) {
			this.balance = balance;
		}
	}
	//----------- | ----------- | ----------- | -----------
	
	public static class CurrentAccount extends Account
	{
		private int threshold;
		
		public CurrentAccount(int ownerNumber, int balance, int threshold)
		{
			super(ownerNumber, balance);
			this.threshold = threshold;
		}
		//Géneration des getters/setters nessessaires
		public int getThreshold() {
			return threshold;
		}

		public void setThreshold(int threshold) {
			this.threshold = threshold;
		}
		
		@Override
		public String toString() 
		{
			return "CurrentAccount | Threshold = " + threshold;	
		}
	}
	//----------- | ----------- | ----------- | -----------
	
	public static class SavingAccount extends Account
	{
		private float interest;
		
		public SavingAccount(int ownerNumber, int balance, float interest)
		{
			super(ownerNumber, balance);
			this.interest = interest;
		}
		//Géneration des getters/setters nessessaires
		public float getInterest() {
			return interest;
		}
		public void setInterest(float interest) {
			this.interest = interest;
		}
		@Override
		public String toString()
		{
			return "SavingAccount | interest =" + interest;
		}
	}
	//----------- | ----------- | ----------- | -----------
	
	public static class Customer
	{
		private static int cptCustomer = 0;
		private int idCustomer;
		private String name;
		
		public Customer(String name)
		{
			this.name = name;
			this.idCustomer = cptCustomer;
			cptCustomer ++;
		}
		//Géneration des getters/setters nessessaires

		public int getIdCustomer() {
			return idCustomer;
		}
		public void setIdCustomer(int idCustomer) {
			this.idCustomer = idCustomer;
		}

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
		@Override
		public String toString()
		{
			return " idCustomer | =" + idCustomer + "Name | = " + name ;
		}
	}
	//----------- | ----------- | ----------- | -----------
	
	public static class Bank
	{
		ArrayList<Customer> customerArray = new ArrayList<Customer>();
		ArrayList<CurrentAccount> currentAccountArray = new ArrayList<CurrentAccount>();
		ArrayList<SavingAccount> savingAccountArray = new ArrayList<SavingAccount>();
		ArrayList<String> tradeArray = new ArrayList<String>();
		
		public Bank() {} //Constructeur non utilisé
		
		public void addCustomer(Customer c1)
		{
			this.customerArray.add(c1);
		}
		
		public void addCurrentAccount(Customer c1, int threshold, int money)
		{
			for(int i = 0 ; i < customerArray.size() ; i++)
			{
				if(customerArray.get(i)==c1)
				{
					currentAccountArray.add(new CurrentAccount(c1.getIdCustomer(),threshold,money));
				}
			}
		}
		
		public void addSavingAccount(Customer c1, int money, float interest)
		{
			for(int i=0;i<customerArray.size();i++)
			{
				if(customerArray.get(i)==c1)
				{
					savingAccountArray.add(new SavingAccount(c1.getIdCustomer(),money,interest));
				}
			}	
		}
		
		public void addMoneyOnSavingAccount(Customer c1,  int money)
		{
			for(int i=0;i<savingAccountArray.size();i++)
			{
				if (savingAccountArray.get(i).getOwnerNumber()==c1.getIdCustomer())
				{
					int actual_money=savingAccountArray.get(i).getBalance();
					
					savingAccountArray.get(i).setBalance(actual_money+money);
					String trans = "customer : "+ c1.toString() + " has deposited "+money+ " on his saving account. New balance is "+savingAccountArray.get(i).getBalance();
					tradeArray.add(trans);
					break;
				}
			}
		}
		
		public void addMoneyOnCurrentAccount(Customer c1, int money) 
		{
			for(int i=0;i<currentAccountArray.size();i++)
			{
				if (currentAccountArray.get(i).getOwnerNumber()==c1.getIdCustomer())
				{
					int actual_money=currentAccountArray.get(i).getBalance();
					currentAccountArray.get(i).setBalance(actual_money+money);
					String trans = "customer: "+ c1.toString() + " has deposited "+money+ " on his current account. New balance is "+currentAccountArray.get(i).getBalance();
					tradeArray.add(trans);
					break;
				}
			}
		}
		
		public void takeMoneyOnSavingsAccount(Customer c1, int money) 
		{
			for(int i=0;i<savingAccountArray.size();i++)
			{
				if (savingAccountArray.get(i).getOwnerNumber()==c1.getIdCustomer())
				{
					int actual_money=savingAccountArray.get(i).getBalance();
					savingAccountArray.get(i).setBalance(actual_money-money);	
					
					if((actual_money-money)>=0)
					{
						currentAccountArray.get(i).setBalance(actual_money-money);
						String trans = "customer: "+ c1.toString() + " has withdrawn "+money+ " to his saving account. New balance is "+savingAccountArray.get(i).getBalance();
						tradeArray.add(trans);
						break;
					}
					else
					{
						String trans = "customer: "+ c1.toString() + " has not sufficient credit to withdraw "+money+ " from his current account";
						tradeArray.add(trans);
						break;
					}
					
				}
			}
		}
		
		public void takeMoneyOnCurrentAccount(Customer c1, int money) 
		{
			for(int i=0;i<currentAccountArray.size();i++)
			{
				if (currentAccountArray.get(i).getOwnerNumber()==c1.getIdCustomer())
				{
					int actual_money=currentAccountArray.get(i).getBalance();
					if((actual_money-money)>currentAccountArray.get(i).threshold)
					{
						currentAccountArray.get(i).setBalance(actual_money-money);
						String trans = "customer: "+ c1.toString() + " has withdrawn "+money+ " from his current account. New balance is "+currentAccountArray.get(i).getBalance();
						tradeArray.add(trans);
						break;
					}
					else
					{
						String trans = "customer: "+ c1.toString() + " has not sufficient credit to withdraw "+money+ " from his current account";
						tradeArray.add(trans);
						break;
					}	
					
				}
			}
		}

		@Override
		public String toString() 
		{
			String el="\n";
			for (int i=0;i<this.tradeArray.size();i++)
			{
				el = el + "---- "+tradeArray.get(i)+"\n";
			}
			return el;
		}
	}


public static void main (String Args[])
	{
		// creating customers Smith and Davies
		Customer c1 = new Customer ("Smith");
		Customer c2 = new Customer ("Davies");
		// creating the Bank
		Bank myBank = new Bank();
		// adding customer Smith to the Bank and adding a CurrentAccount with threshold=500 to him
		myBank.addCustomer(c1);
		myBank.addCurrentAccount(c1, 500, 0);
		// adding customer Davies to the Bank and adding a CurrentAccount with threshold=300 to him
		myBank.addCustomer(c2);
		myBank.addCurrentAccount(c2, 300, 0);
		// adding a SavingsAccount to Mr. Smith with interest−rate=3.0
		myBank.addSavingAccount(c1, 0, 3);
		myBank.addSavingAccount(c1, 0, 3);
		// displaying the status of the bank
		System.out.println(myBank);
		System.out.println("******************************************\n");
		System.out.println("current account list:" + myBank.currentAccountArray + "\n");
		
		// adding 1000 Euro to the current−account of Mr. Smith
		myBank.addMoneyOnCurrentAccount(c1, 1000);
		myBank.addMoneyOnCurrentAccount(c2, 0);
		// remove 100 Euro from the current account of Mr. Smith
		myBank.takeMoneyOnCurrentAccount(c1, 100);
		// remove 2000 Euro from the current account of Mr. Smith
		myBank.takeMoneyOnCurrentAccount(c1, 2000);
		// adding 200 Euro to the savings−account of Mr. Smith
		myBank.addMoneyOnSavingAccount(c1, 200);
		// adding 800 to the current−account of Mr. Davies
		myBank.addMoneyOnCurrentAccount(c2, 800);
		// display the new state of the bank
		System.out.println("\n" + myBank);
	}
}