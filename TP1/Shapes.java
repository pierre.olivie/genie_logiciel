package defaultPackage;
import java.util.ArrayList;

public class MainClassExercice2 
{
	public static abstract class Shape
	{
		private String color="red";
		private boolean filled=true;
		//Constructeur Abstrait
		public Shape()
		{
			this.color="purple";
		}
		public Shape(String color, boolean filled)
		{
			this.color=color;
			this.filled=filled;
		}
		//Déclaration des f() héritées
		public abstract double getArea();
		
		public abstract double getPerimeter();
		
		public void setColor(String color) {this.color=color;}
		
		public void setFilled(boolean filled) {this.filled=filled;}
		
		public String getColor() {return this.color;}
		
		public boolean isFilled() {return this.filled;}
		
		public String filledString()
		{
			if (this.filled) return "filled";
			else return "not-filled";
		}
		
		public String toString() { return "shape color "+this.color+" + "+filledString();}
	}
	
	public static class Circle extends Shape
	{
		private double radius = 1.0;
		
		public Circle() {};
		
		public Circle(double radius) {this.radius=radius;};
		
		public Circle(double radius,String color, boolean filled) 
		{
			super(color,filled);
			this.radius=radius;
		};
		
		public double getRadius() {return this.radius;}
		public void setRadius(double radius) {this.radius=radius;}
		
		@Override
		public double getArea() 
		{
			return 3.14*this.radius*this.radius;
		}// Pi Estimation

		@Override
		public double getPerimeter() 
		{
			return 2*3.14*this.radius;
		}
		
		@Override
		public String toString() 
		{
			return "circle: radius "+this.radius+" which is a subclass of"+ super.toString();
		}
	}
	
	
	public static  class Rectangle extends Shape
	{
		private double width=1.0;
		private double length=1.0;
		
		public Rectangle() {};
		public Rectangle(double width,double length)
		{
			this.length=length;
			this.width=width;
		}
		public Rectangle(double width,double length, String color, boolean filled)
		{
			super(color,filled);
			this.length=length;
			this.width=width;
		}
		
		public double getWidth()
		{
			return this.width;
		}
		
		public void setWidth(double width)
		{
			this.width=width;
		}
		
		public double getLength()
		{
			return this.length;
		}
		
		public void setLength(double length)
		{
			this.length=length;
		}

		@Override
		public double getArea() 
		{
			return this.length * this.width;	
		}

		@Override
		public double getPerimeter() {
			
			return this.width*2+this.length*2;
		}
		
		@Override
		public String toString()
		{
			return "Rectangle: length "+this.length+" and width " +this.width+" which is a subclass of "+ super.toString();
		}
	}
	
	public static class Square extends Rectangle
	{
		public Square() {};
		public Square(double side)
		{
			setLength(side);
			
		}
		public Square(double side,String color, boolean filled)
		{
			setColor(color);
			setLength(side);
			setFilled(filled);
		}
		
		public double getSide()
		{
			return getLength();
		}
		
		public void setSide(double side)
		{
			setLength(side);
		}
		
		
		@Override
		public double getArea() 
		{
			
			return getLength()*getLength();
		}

		@Override
		public double getPerimeter() {
			
			
			return getLength()*4;
		}
		
		@Override
		public String toString()
		{
			return "Square: side "+getLength()+" which is a subclass of "+ super.toString();
		}
	}
	
	public static void main(String[] args)
	{
		ArrayList<Shape> shapes = new ArrayList<Shape>();
		// create 3 circles
		Circle circle1 = new Circle(); // a circle with radius 1.0
		Circle circle2 = new Circle(3.2); // a circle of radius 3.2
		Circle circle3 = new Circle(4,"green",true); // a circle of radius 4 color green and filled in
		// creating 3 rectangles
		Rectangle rectangle1 = new Rectangle();
		Rectangle rectangle2 = new Rectangle(2,4);
		Rectangle rectangle3 = new Rectangle(1,3,"yellow",false);
		// creating 3 squares
		Square square1 = new Square();
		Square square2 = new Square(6);
		Square square3 = new Square(2,"black",false);
		shapes.add(square1);
		shapes.add(square2);
		shapes.add(square3);
		shapes.add(circle1);
		shapes.add(circle2);
		shapes.add(circle3);
		shapes.add(rectangle1);
		shapes.add(rectangle2);
		shapes.add(rectangle3);
		for(Shape s:shapes) 
		{
			System.out.print(" area=" + s.getArea() + " perimeter: " + s.getPerimeter() + " ");
			System.out.println(s);
		}
	}
}
