package defaultPackage2;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class MainClassExercice1 
{
	
	public abstract class Shape {
	    private ShapeSortingStrategy sortingStrategy;
	    
	    // Autres attributs et méthodes

	    public void setSortingStrategy(ShapeSortingStrategy sortingStrategy) {
	        this.sortingStrategy = sortingStrategy;
	    }

	    public void sort() {
	        sortingStrategy.sort(getShapes());
	    }

	    public abstract List<Shape> getShapes();
	    
	}
	
	public interface ShapeSortingStrategy {
	    void sort(List<Shape> shapes);
	}
	
	public class LongestPerimeterSortingStrategy implements ShapeSortingStrategy {
	    @Override
	    public void sort(List<Shape> shapes) {
	        shapes.sort(Comparator.comparingDouble(shape -> {
	            if (shape instanceof Circle) {
	                return 2 * ((Circle) shape).getRadius() * Math.PI;
	            } else if (shape instanceof Rectangle) {
	                return 2 * (((Rectangle) shape).getWidth() + ((Rectangle) shape).getLength());
	            } else if (shape instanceof Triangle) {
	                return ((Triangle) shape).getArrete1() + ((Triangle) shape).getArrete2() + ((Triangle) shape).getArrete3();
	            } else if (shape instanceof Square) {
	                return 4 * ((Square) shape).getSize();
	            } else {
	                return 0;
	            }
	        }).reversed());
	    }
	}

}

