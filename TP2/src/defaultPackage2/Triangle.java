package defaultPackage2;

import defaultPackage.MainClassExercice1.Shape;

public class Triangle implements Shape 
{
	private Point c1;
	private Point c2;
	private Point c3;
	private double arrete1;
	private double arrete2;
	private double arrete3;
	
	
	public Triangle(Point p1, Point p2, Point p3) throws BadShapeCreationException
	{
		if (p1.equals(p2) || p1.equals(p3) || p2.equals(p3)) {
            throw new BadShapeCreationException("The three points defining the triangle must be distinct.");
		}
		
		c1 = p1;
		c2 = p2;
		c3 = p3;
		arrete1 = Math.sqrt((p1.getCoordonneeX()-p2.getCoordonneeX())*(p1.getCoordonneeX()-p2.getCoordonneeX()) + (p1.getCoordonneeY()-p2.getCoordonneeY())*(p1.getCoordonneeY()-p2.getCoordonneeY()));
		arrete2 = Math.sqrt((p1.getCoordonneeX()-p3.getCoordonneeX())*(p1.getCoordonneeX()-p3.getCoordonneeX()) + (p1.getCoordonneeY()-p3.getCoordonneeY())*(p1.getCoordonneeY()-p3.getCoordonneeY()));
		arrete3 = Math.sqrt((p3.getCoordonneeX()-p2.getCoordonneeX())*(p3.getCoordonneeX()-p2.getCoordonneeX()) + (p3.getCoordonneeY()-p2.getCoordonneeY())*(p3.getCoordonneeY()-p2.getCoordonneeY()));	
	}
	
	
	
	public double getArrete1() {
		return arrete1;
	}



	public void setArrete1(double arrete1) {
		this.arrete1 = arrete1;
	}



	public double getArrete2() {
		return arrete2;
	}



	public void setArrete2(double arrete2) {
		this.arrete2 = arrete2;
	}



	public double getArrete3() {
		return arrete3;
	}



	public void setArrete3(double arrete3) {
		this.arrete3 = arrete3;
	}



	public double area() 
	{
		double perimeter = perimeter();
		return Math.sqrt(perimeter*(perimeter-arrete1)*(perimeter-arrete2)*(perimeter-arrete3));
	}
	
	public double perimeter() 
	{
		return arrete1 + arrete2 + arrete3;
	}
	
	public double maxSide() {
		return Math.max(Math.max(arrete1, arrete2), arrete3);
	}
	
	public double minSide() {
		return Math.min(Math.min(arrete1, arrete2), arrete3);
	}
}
