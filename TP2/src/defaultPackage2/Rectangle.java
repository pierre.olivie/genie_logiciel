package defaultPackage2;

import defaultPackage.MainClassExercice1.Shape;

public class Rectangle implements Shape
{
	private double width;
	private double length;
	Point c1;
	
	public Rectangle(Point p, double width, double length) throws BadShapeCreationException
    {
        if (width <= 0 || length <= 0) {
            throw new BadShapeCreationException("width AND length of edges must be positive for Square creation.");
        }
        
        if (width == length){
            throw new BadShapeCreationException("Width and length cannot be equal for Rectangle creation.");
        }
        
		c1 = p;
		this.width = width;
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double area() {
		return width*width;
	}
	public double perimeter() {
		return length*2 + length*2;
	}
	public double maxSide() {
		if (width > length) {
			return width;
		}
		return length;
	}
	public double minSide() {
		if (width > length) {
			return length;
		}
		return width;
	}
}
