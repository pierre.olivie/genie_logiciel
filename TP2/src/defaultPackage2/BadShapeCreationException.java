package defaultPackage2;

public class BadShapeCreationException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public BadShapeCreationException(String message) {
        super(message);
    }
}