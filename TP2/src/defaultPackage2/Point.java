package defaultPackage2;

public class Point
{
	private double coordonneeX;
	private double coordonneeY;
	
	public Point(double coordonneeX, double coordonneeY)
	{
		this.coordonneeX = coordonneeX;
		this.coordonneeY = coordonneeY;
	}

	public double getCoordonneeX() {
		return coordonneeX;
	}
	public void setCoordonneeX(double coordonneeX) {
		this.coordonneeX = coordonneeX;
	}

	public double getCoordonneeY() {
		return coordonneeY;
	}
	public void setCoordonneeY(double coordonneeY) {
		this.coordonneeY = coordonneeY;
	}		
}
