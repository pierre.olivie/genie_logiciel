package defaultPackage;
import defaultPackage.MainClassExercice1.Shape;

public class Circle implements Shape
{
	Point c1;
	private double radius;
	
	public Circle(Point p, double radius) throws BadShapeCreationException {
        if (radius <= 0) 
        {
            throw new BadShapeCreationException("Radius should be positive for Circle creation.");
        }
        
		c1 = p;
		this.radius = radius;
	};
	
	public double area() {
		return Math.PI*radius*radius;
	}
	public double perimeter() {
		return 2*Math.PI*radius;
	}
	public double maxSide() {
		return radius*2;
	}
	public double minSide() {
		return radius;
	}
}
	