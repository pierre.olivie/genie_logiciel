package defaultPackage;
import org.junit.Test;
import static org.junit.Assert.*;

public class ShapeTest {
    
    @Test
    public void testRectangle() throws BadShapeCreationException {
        Rectangle rectangle = new Rectangle(new Point(0, 0), 2, 3);
        assertEquals(6, rectangle.area(), 0.001);
        assertEquals(10, rectangle.perimeter(), 0.001);
        assertEquals(3, rectangle.maxSide(), 0.001);
        assertEquals(2, rectangle.minSide(), 0.001);
    }
    
    @Test(expected = BadShapeCreationException.class)
    public void testRectangleException() throws BadShapeCreationException {
        Rectangle rectangle = new Rectangle(new Point(0, 0), 2, 2);
    }
    
    @Test
    public void testSquare() throws BadShapeCreationException {
        Square square = new Square(new Point(0, 0), 2);
        assertEquals(4, square.area(), 0.001);
        assertEquals(8, square.perimeter(), 0.001);
        assertEquals(2, square.maxSide(), 0.001);
        assertEquals(2, square.minSide(), 0.001);
    }
    
    @Test(expected = BadShapeCreationException.class)
    public void testSquareException() throws BadShapeCreationException {
        Square square = new Square(new Point(0, 0), -2);
    }
    
    @Test
    public void testTriangle() throws BadShapeCreationException {
        Triangle triangle = new Triangle(new Point(0, 0), new Point(0, 3), new Point(4, 0));
        assertEquals(6, triangle.area(), 0.001);
        assertEquals(12, triangle.perimeter(), 0.001);
        assertEquals(5, triangle.maxSide(), 0.001);
        assertEquals(3, triangle.minSide(), 0.001);
    }
    
    @Test(expected = BadShapeCreationException.class)
    public void testTriangleException() throws BadShapeCreationException {
        Triangle triangle = new Triangle(new Point(0, 0), new Point(0, 3), new Point(0, 0));
    }
    
    @Test
    public void testCircle() throws BadShapeCreationException {
        Circle circle = new Circle(new Point(0, 0), 2);
        assertEquals(Math.PI * 4, circle.area(), 0.001);
        assertEquals(Math.PI * 4, circle.perimeter(), 0.001);
        assertEquals(4, circle.maxSide(), 0.001);
        assertEquals(2, circle.minSide(), 0.001);
    }
    
    @Test(expected = BadShapeCreationException.class)
    public void testCircleException() throws BadShapeCreationException {
        Circle circle = new Circle(new Point(0, 0), -2);
    }
    
}