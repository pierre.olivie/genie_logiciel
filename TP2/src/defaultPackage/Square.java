package defaultPackage;

import defaultPackage.MainClassExercice1.Shape;

public class Square implements Shape
{
	private double size;
	Point c1;

	public Square(Point p, double size) throws BadShapeCreationException
    {
        if (size <= 0) {
            throw new BadShapeCreationException("Size of edges must be positive for Square creation.");
        }
        
		c1 = p;
		this.size = size;
	}
	
	public double area() {
		return size*size;
	}
	public double perimeter() {
		return size*4;
	}
	public double maxSide() {
		return size;
	}
	public double minSide() {
		return size;
	}
}
