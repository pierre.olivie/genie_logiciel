package defaultPackage;
import java.util.ArrayList;

public class MainClassExercice1 
{
	public interface Shape {
	    double area();
	    double perimeter();	
	    double maxSide();
	    double minSide();
	}
	
}

