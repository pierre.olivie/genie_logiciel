package defaultPackage3;

public class CalculateurTaxesVisiteurs 
{
	// ---------- | ---------- | ---------- | ---------- | ---------- | ---------- |
	public interface Passenger
	{
		public double visitPrix(Book produit);
		public double visitPrix(Cosmetic produit);
		public double visitPrix(Wine produit);
		public double visitTax(Book produit);
		public double visitTax(Cosmetic produit);
		public double visitTax(Wine produit);
	}	
	// ---------- | ---------- | ---------- | ---------- | ---------- | ---------- |	
	public interface Produit
	{
		public double acceptTaxe(Passenger passenger);
		public double acceptPrix(Passenger passenger);
	}
	// ---------- | ---------- | ---------- | ---------- | ---------- | ---------- |	
	public static class Book implements Produit
	{
		private String titre;
		private double prix;
		
		public Book(String title, double prix)
		{
			this.prix=prix;
			this.titre=titre;
		}
		
		public String getTitre() {
			return titre;
		}
		public void setTitre(String titre) {
			this.titre = titre;
		}

		public double getPrix() {
			return prix;
		}
		public void setPrix(double prix) {
			this.prix = prix;
		}

		@Override
		public double acceptTaxe(CalculateurTaxesVisiteurs.Passenger passenger) {
			return passenger.visitTax(this);
		}
		@Override
		public double acceptPrix(CalculateurTaxesVisiteurs.Passenger passenger) {
			return passenger.visitPrix(this);
		}
	}
	// ---------- | ---------- | ---------- | ---------- | ---------- | ---------- |	
	public static class Wine implements Produit
	{
		private String nom;
		private double prix;
		
		public Wine(String nom, double prix)
		{
			this.prix=prix;
			this.nom=nom;
		}

		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}

		public double getPrix() {
			return prix;
		}
		public void setPrix(double prix) {
			this.prix = prix;
		}

		@Override
		public double acceptTaxe(CalculateurTaxesVisiteurs.Passenger passenger) {
			return passenger.visitTax(this);
		}	
		@Override
		public double acceptPrix(CalculateurTaxesVisiteurs.Passenger passenger) {
			return passenger.visitPrix(this);
		}	
	}
	// ---------- | ---------- | ---------- | ---------- | ---------- | ---------- |	

	public static class Cosmetic implements Produit
	{
		
		private String nom;
		private double prix;
		
		public Cosmetic(String nom, double prix)
		{
			this.prix=prix;
			this.nom=nom;
		}

		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}

		public double getPrix() {
			return prix;
		}
		public void setPrix(double prix) {
			this.prix = prix;
		}

		@Override
		public double acceptTaxe(CalculateurTaxesVisiteurs.Passenger passenger) {
			return passenger.visitTax(this);
		}
		@Override
		public double acceptPrix(CalculateurTaxesVisiteurs.Passenger passenger) {
			return passenger.visitPrix(this);
		}
	}
	// ---------- | ---------- | ---------- | ---------- | ---------- | ---------- |
	public static class NormalPassenger implements Passenger
	{
		@Override
		public double visitTax(CalculateurTaxesVisiteurs.Book produit) {
			return produit.getPrix()*0.1;
		}
		@Override
		public double visitTax(CalculateurTaxesVisiteurs.Cosmetic produit) {
			return produit.getPrix()*0.3;
		}
		@Override
		public double visitTax(CalculateurTaxesVisiteurs.Wine produit) {
			return produit.getPrix()*0.32;
		}
		@Override
		public double visitPrix(CalculateurTaxesVisiteurs.Book produit) {
			return produit.getPrix();
		}
		@Override
		public double visitPrix(CalculateurTaxesVisiteurs.Cosmetic produit) {
			return produit.getPrix();
		}
		@Override
		public double visitPrix(CalculateurTaxesVisiteurs.Wine produit) {
			return produit.getPrix();
		}
	}
	// ---------- | ---------- | ---------- | ---------- | ---------- | ---------- |
	public static class CorporatePassenger implements Passenger
	{
		@Override
		public double visitTax(CalculateurTaxesVisiteurs.Book produit) {
			return produit.getPrix()*0.07;
		}
		@Override
		public double visitTax(CalculateurTaxesVisiteurs.Cosmetic produit) {
			return produit.getPrix()*0.2;
		}
		@Override
		public double visitTax(CalculateurTaxesVisiteurs.Wine produit) {
			return produit.getPrix()*0.2;
		}
		@Override
		public double visitPrix(CalculateurTaxesVisiteurs.Book produit) {
			return produit.getPrix();
		}
		@Override
		public double visitPrix(CalculateurTaxesVisiteurs.Cosmetic produit) {
			return produit.getPrix();
		}
		@Override
		public double visitPrix(CalculateurTaxesVisiteurs.Wine produit) {
			return produit.getPrix();
		}
	}
	// ---------- | ---------- | ---------- | ---------- | ---------- | ---------- |
	public static class ExecutivePassenger implements Passenger
	{
		@Override
		public double visitTax(CalculateurTaxesVisiteurs.Book produit) {
			return produit.getPrix()*0.05;
		}
		@Override
		public double visitTax(CalculateurTaxesVisiteurs.Cosmetic produit) {
			return produit.getPrix()*0.1;
		}
		@Override
		public double visitTax(CalculateurTaxesVisiteurs.Wine produit) {
			return produit.getPrix()*0.1;
		}
		@Override
		public double visitPrix(CalculateurTaxesVisiteurs.Book produit) {
			return produit.getPrix();
		}
		@Override
		public double visitPrix(CalculateurTaxesVisiteurs.Cosmetic produit) {
			return produit.getPrix();
		}
		@Override
		public double visitPrix(CalculateurTaxesVisiteurs.Wine produit) {
			return produit.getPrix();
		}
	}
	// ---------- | ---------- | ---------- | ---------- | ---------- | ---------- |
	public class Panier
	{	
		public static double calculPrixNormalPassenger(Produit[] panier)
		{
			Passenger passenger = new NormalPassenger();
			double sum = 0;
			for(Produit produit : panier)
			sum = sum + produit.acceptPrix(passenger);
			return sum;
		}
		
		public static double calculTaxeNormalPassenger(Produit[] panier)
		{
			Passenger passenger = new NormalPassenger();
			double sum = 0;
			for(Produit produit : panier)
			sum = sum + produit.acceptTaxe(passenger);
			return sum;
		}
		
		public static double calculPrixCorporatePassenger(Produit[] panier)
		{
			Passenger passenger = new CorporatePassenger();
			double sum = 0;
			for(Produit produit : panier)
			sum = sum + produit.acceptPrix(passenger);
			return sum;
		}
		
		public static double calculTaxeCorporatePassenger(Produit[] panier)
		{
			Passenger passenger = new CorporatePassenger();
			double sum = 0;
			for(Produit produit : panier)
			sum = sum + produit.acceptTaxe(passenger);
			return sum;
		}
		
		public static double calculPrixExecutivePassenger(Produit[] panier)
		{
			Passenger passenger = new ExecutivePassenger();
			double sum = 0;
			for(Produit produit : panier)
			sum = sum + produit.acceptPrix(passenger);
			return sum;
		}
		
		public static double calculTaxeExecutivePassenger(Produit[] panier)
		{
			Passenger passenger = new ExecutivePassenger();
			double sum = 0;
			for(Produit produit : panier)
			sum = sum + produit.acceptTaxe(passenger);
			return sum;
		}
	}
	// ---------- | ---------- | ---------- | ---------- | ---------- | ---------- |
	public static void main(String[] args)
	{
		Produit[] panier = new Produit[]{new Book("DonnezMoiVacances",20),new Book ("LeJavaPourLesNuls",10)};
		System.out.println("Normal:");
		System.out.println("Prix: " + Panier.calculPrixNormalPassenger(panier));
		System.out.println("Taxes: " + Panier.calculTaxeNormalPassenger(panier));
		System.out.println("Corporate:");
		System.out.println("Prix: " + Panier.calculPrixCorporatePassenger(panier));
		System.out.println("Taxes: " + Panier.calculTaxeCorporatePassenger(panier));
		System.out.println("Executif:");
		System.out.println("Prix: " + Panier.calculPrixExecutivePassenger(panier));
		System.out.println("Taxes: " + Panier.calculTaxeExecutivePassenger(panier));		
	}
}